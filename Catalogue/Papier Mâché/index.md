---
Edition: Editions REPAS
DépotLégal: 2016
Auteur: Christian Vaillant
Titre: Le Papier Mâché
SousTitre: Un restaurant-librairie autogéré (1978-1985)
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-09-9
Pages: 128 pages
Prix: 12
Etat: Disponible
Résumé: |
    Ce livre raconte l’histoire d’un collectif créateur d’un lieu autogéré aux activités multiples (librairie, restaurant, salle de réunions, cinéma, théâtre, lieu d’expositions...), à Nice, de 1978 à 1985. L’essentiel du livre est consacré à la naissance du projet et du collectif, aux activités qui ont été mises en œuvre et à son mode de fonctionnement autogéré. Il a paru important d’y ajouter des éléments de contexte de la société française des années 1970, ou plus anciens, dont le Papier mâché paraît être à la fois la conséquence et l’illustration.

    Ces années 1970 ont été la grande époque, et même la seule, de l’idée d’autogestion en France. Cette idée a très rapidement et complètement disparu dans la deuxième moitié des années 1980. Dans les très rares livres revenant sur la question de l’autogestion en ce début de XXIe siècle, les pages consacrées aux années 1970 parlent essentiellement du PSU et de la CFDT (ce qui est légitime), du PS (ce qui l’est beaucoup moins), de Lip et de quelques luttes autogérées. Mais ils ne parlent pratiquement pas du grand nombre de lieux de vie ou de lieux de travail autogérés qui se sont créés à cette époque, à l'image du Papier mâché.
Tags:
- Autogestion
- Collectif
- Alternatives
- Travail
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/le-papier-mache-christian-vaillant?_ga=2.240267267.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272099-le-papier-mache-un-restaurant-librairie-autogere-1978-1985-christian-vaillant/
Couverture:
Couleurs:
 Texte: '#F7F5B7'
 PageTitres: '#a2151d'
---

## L'auteur

**Christian Vaillant** a été l’un de ceux à l’origine de ce projet, le seul de ses salariés continûment de son ouverture à sa fermeture, l’un de ceux qui s’occupèrent des démarches postérieures et probablement l’un de ceux pour lesquels cette histoire fut déterminante. En effet, certains membres de ce collectif, dont l’auteur, après quelques années de salariat dans des entreprises « normales », ont créé à nouveau des structures de travail autogérées fonctionnant sur les mêmes principes que le Papier mâché.


## Extraits

> En principe, nous devons décider collectivement du contenu de la librairie, même titre par titre. Évidemment, une « commission » est créée car nous n’allons pas discuter à trente, titre par titre, les 3 ou 4 000 livres que nous allons avoir au départ. Des listes sont établies. Nous devons être bien sûr une librairie avec un fond et pas seulement des nouveautés. (...)
>
> En principe également, nous devons « tourner » sur le poste de libraire, aussi bien salariés que bénévoles. Dans la pratique, une spécialisation s’instaure après la phase initiale. En dehors de la simple permanence, la gestion de la librairie nécessite une compétence technique (connaissance des éditeurs, des circuits de distribution, des conditions de vente…) et un suivi difficiles à acquérir par une présence ponctuelle. Assez rapidement, un des salariés, celui qui avait le plus pour projet d’ouvrir une librairie et qui a fait un stage dans une librairie parisienne avant l’ouverture, consacre plus de temps à la librairie qu’au restaurant. Les autres salariés et les bénévoles n’assurent le plus souvent que des permanences où ils s’ennuient un peu, seuls dans la librairie, alors qu’il y a toujours quelque chose à faire avec les autres à dix mètres dans le restaurant ; un bénévolat mixte sera alors fréquent : épluchage de légumes tout en surveillant la librairie.
> -- pages 56-57

